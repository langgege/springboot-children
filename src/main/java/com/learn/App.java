package com.learn;

import org.springframework.boot.SpringApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
/**
 * springboot运行流程分析
 * 1：deduceWebApplicationType()检测是否时web环境
 * 2：加载classpath下META-INF/spring.factories中的所有  ApplicationContextInitializer
 * 3：加载classpath下META-INF/spring.factories中的所有  ApplicationListener
 * 4：推断main方法所在类
 * 5:执行run方法
 * 6：设置java.awt.headless系统变量
 * 7：加载classpath下META-INF/spring.factories中的所有 SpringApplicationRunListener
 * 8：执行所有SpringApplicationRunListener的started()方法
 * 9：实例化ApplicationArguments对象
 * 10：创建environment，运行环境
 * 11：配置environment，主要是把run方法的参数配置到environment
 * 12：执行所有SpringApplicationRunListener的environmentPrepared()方法
 * 13：如果不是web环境，但是是web的environment，则把这个web的environment转换成标准的environment
 * 14：打印banner
 * 15：实例化ApplicationContext对象，如果是web环境则实例化web.servlet.context.AnnotationConfigServletWebServerApplicationContext对象
 * 		否则实例化annotation.AnnotationConfigApplicationContext对象
 * 16：回调所有ApplicationContextInitializer方
 * 17：依次往容器中注入ApplicationArguments，banner
 * 18：把所有的源加载到ApplicationContext
 * 				类似：AnnotationConfigApplicationContext ac = new AnnotationConfigApplicationContext();
        				ac.register(annotatedClasses);
 * 19：执行所有SpringApplicationRunListener的contextLoaded()方法
 * 20：执行context的refresh方法 			类似：ac.refresh();
 * 			:容器启动好了
 * 21：回调，获取容器中所有的ApplicationRunner，CommandLineRunner接口，排序，依次调用
 * 22：执行所有SpringApplicationRunListener的finished()；springboot启动完成
 * 		
 * 
 * @author Romanceling
 *
 */
public class App {
    public static void main( String[] args ){
        ConfigurableApplicationContext context = SpringApplication.run(App.class, args);
        context.close();
    }
}
